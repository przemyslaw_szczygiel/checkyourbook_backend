package com.example.demo.control;

import com.example.demo.data.Monograph;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.Table;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete


public interface MonographsRepository extends CrudRepository<Monograph, Integer> {

}
