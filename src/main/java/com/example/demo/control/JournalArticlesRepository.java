package com.example.demo.control;

import com.example.demo.data.JournalArticle;
import org.springframework.data.repository.CrudRepository;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete


public interface JournalArticlesRepository extends CrudRepository<JournalArticle, Integer> {

}