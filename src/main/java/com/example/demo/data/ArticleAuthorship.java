/*package com.example.demo.data;

import javax.persistence.*;

@Table(name = "article_authorships")
@Entity // This tells Hibernate to make a table out of this class
public class ArticleAuthorship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int articleAuthorshipId;
    @ManyToOne
    @JoinColumn(name = "articleId")
    private JournalArticle journalArticle;

    @ManyToOne
    @JoinColumn(name = "authorId")
    private Author author;

    public int getArticleAuthorshipId() {
        return articleAuthorshipId;
    }

    public void setArticleAuthorshipId(int articleAuthorshipId) {
        this.articleAuthorshipId = articleAuthorshipId;
    }

    public JournalArticle getJournalArticle() {
        return journalArticle;
    }

    public void setJournalArticle(JournalArticle journalArticle) {
        this.journalArticle = journalArticle;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
*/