package com.example.demo.data;

import javax.persistence.*;

@Table(name = "journal_articles")
@Entity // This tells Hibernate to make a table out of this class
public class JournalArticle {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int articleId;
        private String title;

        @OneToOne
        @JoinColumn(name = "authorId")
        private Author author;

        @OneToOne
        @JoinColumn(name = "journalId")
        private Journal journal;


    public int getArticleId() {
        return articleId;
    }

    public void setId(int id) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }
}


