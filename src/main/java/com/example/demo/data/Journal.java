package com.example.demo.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;

@Table(name = "journals")
@Entity // This tells Hibernate to make a table out of this class
public class Journal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int journalId;
    private String title;
    private String issue;
    private int year;

    public Journal(){

    }


    public Journal(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Journal journal = mapper.reader().forType(Journal.class).readValue(json);
            this.journalId = journal.journalId;
            this.title = journal.title;
            this.issue = journal.issue;
            this.year = journal.year;
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.journalId = journalId;
        this.title = title;
        this.issue = issue;
        this.year = year;
    }

    public Journal(int journalId, String title, String issue, int year){
        this.journalId = journalId;
        this.title = title;
        this.issue = issue;
        this.year = year;
    }




    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
