package com.example.demo.boundary;

import com.example.demo.control.*;
import com.example.demo.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

// dostep do backendu

@CrossOrigin
@RestController
public class DatabaseController {


    @Autowired
    private AuthorsRepository authorsRepository;

    @Autowired
    private JournalArticlesRepository journalArticlesRepository;

    @Autowired
    private JournalsRepository journalsRepository;

    @Autowired
    private MonographsRepository monographsRepository;

    // zapisywanie autorów

    @PostMapping(path = "/authors", consumes = "application/json")
    public void postAuthor(@RequestBody Author author)
    {
        authorsRepository.save(author);
    }

    @RequestMapping(path = "/authors")
    public List<Author> getAllAuthors()
    {
        Iterable<Author> authors = authorsRepository.findAll();
        List<Author> authorList = new ArrayList<>();
        Iterator<Author> iterator = authors.iterator();
        while(iterator.hasNext()) {
            authorList.add(iterator.next());
        }
        return authorList;
    }

// zapisywanie czasopism

    @PostMapping(path = "/journals", consumes = "application/json")
    public void postJournal(@RequestBody Journal journal)
    {
        journalsRepository.save(journal);
    }

    @RequestMapping(path = "/journals")
    public List<Journal> getAllJournals()
    {
        Iterable<Journal> journals = journalsRepository.findAll();
        List<Journal> journalList = new ArrayList<>();
        Iterator<Journal> iterator = journals.iterator();
        while(iterator.hasNext()) {
            journalList.add(iterator.next());
        }
        return journalList;
    }

    // zapisywanie artykułów

    @PostMapping(path = "/journalArticles", consumes = "application/json")
    public void postJournalArticle(@RequestBody JournalArticle journalArticle)
    {
        journalArticlesRepository.save(journalArticle);
    }

    // dostęp do, odczyt artykułów

    @RequestMapping(path = "/journalArticles")
    public List<JournalArticle> getAllJournalArticles()
    {
        Iterable<JournalArticle> journalArticles = journalArticlesRepository.findAll();
        List<JournalArticle> journalArticleList = new ArrayList<>();
        Iterator<JournalArticle> iterator = journalArticles.iterator();
        while(iterator.hasNext()) {
            journalArticleList.add(iterator.next());
        }
        return journalArticleList;
    }

    // zapisywanie monografii

    @PostMapping (path = "/monographs", consumes = "application/json")
    public void postMonograph(@RequestBody Monograph monograph)
    {
        monographsRepository.save(monograph);
    }

    // odczyt monografii

    @RequestMapping(path = "/monographs")
    public List<Monograph> getAllMonographs()
    {
        Iterable<Monograph> monographs = monographsRepository.findAll();
        List<Monograph> monographList = new ArrayList<>();
        Iterator<Monograph> iterator = monographs.iterator();
        while(iterator.hasNext()) {
            monographList.add(iterator.next());
        }
        return monographList;
    }

    // usuwanie monografii

    @DeleteMapping(path = "/monographs/{id}")
    public void deleteMonograph(@PathVariable int id)
    {
        monographsRepository.deleteById(id);
    }

    // usuwanie artykułu

    @DeleteMapping(path = "/journalArticles/{id}")
    public void deleteJournalArticle(@PathVariable int id)
    {
        journalArticlesRepository.deleteById(id);
    }

    // edytowanie bazy z monografiami

    @PutMapping(path = "/monographs")
    public void putMonograph(@RequestBody Monograph monograph)
    {monographsRepository.save(monograph);}

// edycja bazy czasopism

    @PutMapping(path = "/journalArticles")
        public void putJournalArticle(@RequestBody JournalArticle journalArticle)
    {journalArticlesRepository.save(journalArticle);}

}

